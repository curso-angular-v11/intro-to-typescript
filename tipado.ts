// Boolean
let esValido: boolean = true;
esValido = false;

//Number
let nombre: string = 'Gustavo';
let apellido: string = 'López';

let nombre_completo: string = `${nombre} ${apellido}`;

//Array
let numeros: number[] = [12, 3, 4, 5, 6, 7];
let numeros2: number[] = [3, 1, 65, 2];

//Tupla
let sitio: [string, number] = ["Casa", 2];

//Enum
enum Estado {
    Offline,
    Indefinido,
    Online
}

let stat: Estado = Estado.Online;
console.log(stat);

//Unknown
let sinTipo: unknown = 'Hola';
sinTipo = 32;
sinTipo = true;

let nuevaCadena: string = 'esto es otra cadena';
//nuevaCadena = sinTipo; no es posible hacer esto

//Any
let tipoIndefinido: any = 'esto es un mensaje';
nuevaCadena = tipoIndefinido;

// Void
function logger(): void {
    console.log('Logger');
}