// function sumar(a, b) {
//     return a + b;
// }
// Las tres declaraciones siguientes son validas
const sumar = function (a, b) {
    return a + b;
};
// Esta es la más usada
const sumar2 = function (a, b) {
    return a + b;
};
const sumar3 = function (a, b) {
    return a + b;
};
// Parametros opcionales y valores por defecto
function nombreCompleto(nombre, apellido) {
    if (apellido)
        return nombre + " " + apellido;
    else
        return nombre;
}
console.log(nombreCompleto('Pedro'));
console.log(nombreCompleto('Mario', 'Romero'));
// Parámetros REST
function nombreCompleto2(nombre, ...restoNombre) {
    return nombre + ' ' + restoNombre.join(' ');
}
console.log(nombreCompleto2('Ana', 'Maria', 'Dolores', 'Garcia', 'Santos'));
